﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueStarter : MonoBehaviour
{
    public Dialogue dialogue;
    public Text dialogueText;
    bool dialogueHasStarted = false;


    public void StartDialogue() {
        if (dialogueHasStarted)
            return;

        StartCoroutine(ShowDialogue());
    }

    IEnumerator ShowDialogue() {
        dialogueHasStarted = true;

        for (int i = 0; i < dialogue.lineContent.Length; i++)
        {
            yield return new WaitForSeconds (dialogue.lineDelay[i]);
            dialogueText.text = dialogue.lineContent[i];
            yield return new WaitForSeconds (dialogue.lineDuration[i]);
            dialogueText.text = "";
        }

        dialogueHasStarted = false;
    }
}
