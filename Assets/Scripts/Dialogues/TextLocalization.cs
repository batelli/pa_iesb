﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "String0000", menuName = "RPG Assets/new Language")]
public class TextLocalization : ScriptableObject
{
    public string textID;
    public string stringText;
}
