﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Weapon : MonoBehaviour
{
    public Damage damage = new Damage(1, ElementOfDamage.Neutral, TypeOfDamage.Melee, 0);


    protected virtual void OnTriggerEnter(Collider other) {
        var being = other.GetComponent<LivingBeing>();
        if (being)
            being.TakeDamage(damage);
    }
}
