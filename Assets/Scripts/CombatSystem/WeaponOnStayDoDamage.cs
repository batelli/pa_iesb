﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponOnStayDoDamage : Weapon
{
    public float timeBetweenDamage;
    protected List<LivingBeing> beingList = new List<LivingBeing>();


    protected override void OnTriggerEnter(Collider other) {
        var being = other.GetComponent<LivingBeing>();
        if (being) {
            being.TakeDamage(damage);
            beingList.Add(being);
            if (beingList.Count == 1)
                StartCoroutine(DoDamageWhileOnTrigger());
        }
    }

    protected virtual void OnTriggerExit(Collider other) {
        var being = other.GetComponent<LivingBeing>();
        if (being) {
            beingList.Remove(being);
        }
    }

    IEnumerator DoDamageWhileOnTrigger() {
        WaitForSeconds wfs = new WaitForSeconds(timeBetweenDamage);

        while (beingList.Count > 0) {
            yield return wfs;

            for (int i = 0; i < beingList.Count; i++)
                beingList[i].TakeDamage(damage);

            if (beingList.Count == 0)
                yield break;
        }
    }
}
