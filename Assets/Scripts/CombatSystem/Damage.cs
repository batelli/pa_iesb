﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum TypeOfDamage { Melee, Ranged, Magical, Special }
public enum ElementOfDamage { Neutral, Fire, Water, Earth, Air }

public class Damage {

    public int damageAmount;
    public TypeOfDamage damageType;
    public ElementOfDamage damageElement;
    public float critChance;


    public Damage(int damageValue) {
        damageAmount = damageValue;
        damageElement = ElementOfDamage.Neutral;
        damageType = TypeOfDamage.Melee;
        critChance = 0;
    }

    public Damage(int damageValue, ElementOfDamage element, TypeOfDamage type, float critical) {
        damageAmount = damageValue;
        damageElement = element;
        damageType = type;
        critChance = critical;
    }
}
