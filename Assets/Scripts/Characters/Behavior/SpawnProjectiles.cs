﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnProjectiles : StateMachineBehaviour
{

    public GameObject projectilePrefab;
    public float timeBetweenProjectiles=1;
    float timer = 0;
    public Vector3 spawnPosition = new Vector3(0, 5, 0);
    public Vector3 randomRadius = new Vector3(5, 0, 5);
    public float forceOnProjectile = 0;


    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer = 0;    
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer+=Time.deltaTime;
        if (timer > timeBetweenProjectiles) {
            timer = 0;
            GameObject bullet = Instantiate(projectilePrefab, animator.transform.position + spawnPosition 
                + new Vector3(Random.Range(-randomRadius.x, randomRadius.x), Random.Range(-randomRadius.y, randomRadius.y), 
                Random.Range(-randomRadius.z, randomRadius.z)), Quaternion.identity);

            Rigidbody rig = bullet.GetComponent<Rigidbody>();
            if (rig)
                rig.AddForce(forceOnProjectile * animator.transform.forward);
        }
    }
}
