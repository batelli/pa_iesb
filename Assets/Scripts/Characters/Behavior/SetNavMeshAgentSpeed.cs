﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetNavMeshAgentSpeed : StateMachineBehaviour
{
    public float[] speeds;
    public float[] distances;
    public string distanceParameter;
    float distance;
    Enemy enemy;


    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy = animator.GetComponent<Enemy>();
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        distance = animator.GetFloat(distanceParameter);
        for (int i = 0; i < speeds.Length; i++)
        {
            if (distance < distances[i]) {
                enemy.agent.speed = speeds[i];
                break;
            }
        }
        
    }
}
