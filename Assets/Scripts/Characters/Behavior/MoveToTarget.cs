﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveToTarget : StateMachineBehaviour
{
    Enemy enemy;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy = animator.GetComponent<Enemy>();
        enemy.agent.isStopped = false;
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if(enemy.target)
            enemy.agent.SetDestination(enemy.target.position);
    }
}
