﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetRandomTrigger : StateMachineBehaviour
{
    public string[] triggers;
    public Vector2 minMaxTime = new Vector2(1, 3);
    float timer = 0;


    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer = Random.Range(minMaxTime.x, minMaxTime.y);
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer -= Time.deltaTime;
        if (timer <= 0) {
            timer = 10;
            animator.SetTrigger(triggers[Random.Range(0, triggers.Length)]);
        }
    }
}
