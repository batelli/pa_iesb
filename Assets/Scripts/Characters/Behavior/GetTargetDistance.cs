﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetTargetDistance : StateMachineBehaviour
{
    Enemy enemy;
    public string animatorParameter;


    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemy = animator.GetComponent<Enemy>();
    }
    
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (enemy.target)
            animator.SetFloat(animatorParameter, Vector3.Distance(new Vector3 (animator.transform.position.x, 0, animator.transform.position.z),
                                                                    new Vector3(enemy.target.position.x, 0, enemy.target.position.z)));
    }
}
