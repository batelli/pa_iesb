﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetRandomInteger : StateMachineBehaviour
{
    public float timeToSortAnim = 5;
    float timer = 0;
    public int minValue = 1;
    public int maxValue = 4;
    public string animatorParameter;


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer = 0;
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        timer+=Time.deltaTime;
        if (timer > timeToSortAnim) {
            animator.SetInteger(animatorParameter, Random.Range(minValue, maxValue+1));
            timer = 0;
        }
    }
}
