﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComplexLivingBeing : LivingBeing {

    protected Rigidbody rigid;
    public List<TypeOfDamage> typeResistance = new List<TypeOfDamage>();
    public List<ElementOfDamage> elementResistance;
    public int armorValue = 0;
    public delegate void Damaged(float health);
    public Damaged BeingWasDamaged;


    protected override void Awake() {
        base.Awake();
        rigid = GetComponent<Rigidbody>();
    }

    public override void TakeDamage(Damage damage) {
        if (dead  ||  typeResistance.Contains(damage.damageType)  ||  elementResistance.Contains(damage.damageElement))
            return;

        float chance = Random.Range(0f, 1f);
        if (chance <= damage.critChance)
            damage.damageAmount *= 2;

        damage.damageAmount -= armorValue;
        damage.damageAmount = Mathf.Max(1, damage.damageAmount);

        if (BeingWasDamaged != null)
            BeingWasDamaged((float)hitPointsCurrent/hitPointsMax);

        hitPointsCurrent -= damage.damageAmount;
        if (dead)
            Death();
    }
}
