﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum AIBehaviors { IDLE, PATROL, RUN_TO_TARGET, ATTACK, DEAD }

public class EnemyStateMachine : ComplexLivingBeing
{

    public AIBehaviors currentBehavior = AIBehaviors.IDLE;
    float timer = 0;
    public NavMeshAgent agent;
    Vector3 initialPosition;
    public float patrolRadius = 5;
    public Transform target;
    public float distanceToAttack = 3;
    public GameObject weapon;


    protected override void Awake() {
        base.Awake();
        agent = GetComponent<NavMeshAgent>();
        initialPosition = transform.position;
    }

    private void Update() {
        switch (currentBehavior)
        {
            case AIBehaviors.IDLE:
                IdleUpdate();
                break;
            case AIBehaviors.PATROL:
                PatrolUpdate();
                break;
            case AIBehaviors.RUN_TO_TARGET:
                RunToTargetUpdate();
                break;
            case AIBehaviors.ATTACK:
                AttackUpdate();
                break;
        }
    }

    void IdleUpdate() {
        timer += Time.deltaTime;
        if (timer > 3) {
            timer = 0;
            currentBehavior = AIBehaviors.PATROL;
            agent.SetDestination(initialPosition + 
                new Vector3(Random.Range(-patrolRadius, patrolRadius), 0, Random.Range(-patrolRadius, patrolRadius)));
        }
    }

    void PatrolUpdate() {
        if (agent.remainingDistance <= agent.stoppingDistance) {
            currentBehavior = AIBehaviors.IDLE;
        }
    }

    void RunToTargetUpdate() {
        if (!target) {
            timer = 0;
            currentBehavior = AIBehaviors.IDLE;
            return;
        }

        agent.SetDestination(target.position);
        if (agent.remainingDistance <= distanceToAttack) {
            currentBehavior = AIBehaviors.ATTACK;
            timer = 0;
        }
    }

    void AttackUpdate() {
        if (!target) {
            currentBehavior = AIBehaviors.IDLE;
            timer = 0;
            return;
        }
        else if (Vector3.Distance(target.position, transform.position) > distanceToAttack) {
            currentBehavior = AIBehaviors.RUN_TO_TARGET;
            timer = 0;
            return;
        }

        timer += Time.deltaTime;
        if (timer > 2f) {
            weapon.SetActive(true);
            timer = 0;
        }
    }

    protected override void Death() {
        base.Death();
        currentBehavior = AIBehaviors.DEAD;
        agent.isStopped = true;
    }

    public void SetNewTarget(Transform trans) {
        if (currentBehavior == AIBehaviors.DEAD)
            return;

        target = trans;
        timer = 0;
        currentBehavior = AIBehaviors.RUN_TO_TARGET;
    }

    public void SetNoTarget() {
        if (currentBehavior == AIBehaviors.DEAD)
            return;

        target = null;
        timer = 0;
        currentBehavior = AIBehaviors.IDLE;
    }
}
