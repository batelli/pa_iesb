﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDetector : MonoBehaviour
{
    public GameObject enemy;


    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            enemy.SendMessage("SetNewTarget", other.transform, SendMessageOptions.DontRequireReceiver);
        }
    }

    private void OnTriggerExit(Collider other) {
        enemy.SendMessage("SetNoTarget", SendMessageOptions.DontRequireReceiver);
    }
}
