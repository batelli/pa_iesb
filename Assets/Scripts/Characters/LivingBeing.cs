﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivingBeing : MonoBehaviour
{
    public int hitPointsMax = 10;
    protected int hitPointsCurrent;
    public bool dead { get { return (hitPointsCurrent <= 0); } }


    protected virtual void Awake() {
        CalculateInitialHitPoints();
    }

    public virtual void TakeDamage(Damage damage) {
        if (dead)
            return;

        hitPointsCurrent = hitPointsCurrent - damage.damageAmount;
        if (dead)
            Death();
    }

    protected virtual void Death() {
        hitPointsCurrent = 0;
    }

    protected virtual void CalculateInitialHitPoints() {
        hitPointsCurrent = hitPointsMax;
    }
}
