﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CallAnimationEvents : MonoBehaviour
{
    public UnityEvent[] animEvents;

    public void CallAnimEvent(int index) {
        animEvents[index].Invoke();
    }
}
