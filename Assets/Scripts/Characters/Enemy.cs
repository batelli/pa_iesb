﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : ComplexLivingBeing
{
    public NavMeshAgent agent;
    Vector3 initialPosition;
    public Transform target;
    public Animator animator;


    public void SetNewTarget(Transform trans) {
        target = trans;
        animator.SetBool("Target", true);
    }

    public void SetNoTarget() {
        target = null;
        animator.SetBool("Target", false);
    }

    public override void TakeDamage(Damage damage) {
        base.TakeDamage(damage);

        if (!dead)
            animator.SetTrigger("Damaged");
    }

    protected override void Death() {
        base.Death();
        animator.SetBool("Dead", true);
    }
}
