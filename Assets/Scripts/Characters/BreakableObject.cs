﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class BreakableObject : LivingBeing
{
    public UnityEvent deathEvents;

    protected override void Death() {
        base.Death();
        deathEvents.Invoke();
    }
}
