﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIsGrounded : MonoBehaviour
{
    public bool isGrounded = false;
    int count = 0;

    private void OnTriggerEnter(Collider other) {
        count++;
        if (count > 0)
            isGrounded = true;
    }

    private void OnTriggerExit(Collider other) {
        count--;
        if (count <= 0) {
            isGrounded = false;
            count = 0;
        }
    }
}
