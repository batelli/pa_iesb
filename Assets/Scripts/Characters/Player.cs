﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : ComplexLivingBeing
{
    #region Singleton
    //Singleton
    static Player instance;
    public static Player Instance {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<Player>();

            return instance;
        }
    }

    #endregion Singleton

    public CharacterClass playerClass;
    public float speed = 2;
    public float rotationSpeed = 90;
    public int stepForce = 3000;
    public int jumpForce = 1000;
    public PlayerIsGrounded playerIsGrounded;
    public GameObject weapon;
    public GameObject myCamera;
    public int money;
    


    protected override void Awake() {
        base.Awake();

        instance = this;
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape))
            GameManager.Instance.PauseUnpauseTheGame();

        if (Time.timeScale < 0.01f)
            return;

        //Movimento
        Vector3 vel = new Vector3(0, rigid.velocity.y, 0);
        rigid.velocity = transform.forward * Input.GetAxis("Vertical") * speed * Time.deltaTime;
        rigid.velocity = new Vector3(rigid.velocity.x, vel.y, rigid.velocity.z);


        if (rigid.velocity.magnitude > 1) { //x. y. z
            RaycastHit hitInfo;
            if (Physics.Raycast(transform.position + (transform.forward), Vector3.down, out hitInfo, 0.85f)) {
                rigid.AddForce(transform.up * stepForce * Time.deltaTime);
            }
        }

        if (Input.GetKeyDown(InputManager.Instance.jumpKey)  &&   playerIsGrounded.isGrounded)
            rigid.AddForce(Vector3.up * jumpForce);


        if (Input.GetKeyDown(InputManager.Instance.weaponKey))
            weapon.SetActive(true);


        //Rotacao
        transform.Rotate(transform.up * Input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime);
    }

    protected override void CalculateInitialHitPoints() {
        switch (playerClass)
        {
            case CharacterClass.Paladin:
            case CharacterClass.Warrior:
            case CharacterClass.Ranger:
                hitPointsMax = 10;
                break;
            case CharacterClass.Wizard:
            case CharacterClass.Necromancer:
                hitPointsMax = 4;
                break;
            case CharacterClass.Rogue:
            case CharacterClass.Monk:
            case CharacterClass.Sorcerer:
                hitPointsMax = 6;
                break;
            case CharacterClass.Barbarian:
                hitPointsMax = 12;
                break;
            case CharacterClass.Cleric:
                hitPointsMax = 8;
                break;
            default:
                hitPointsMax = 4;
                break;
        }

        hitPointsCurrent = hitPointsMax;
    }

    public void AddMoney(int amount) {
        money += amount;
        money = Mathf.Clamp(money, 0, 9999);
    }
}
