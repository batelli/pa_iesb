﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    static UIManager instance;
    public static UIManager Instance { get { return instance; } }

    public GameObject pausePanel;



    private void Awake() {
        instance = this;
    }

    public void PauseTheGame() {
        pausePanel.SetActive(true);
    }

    public void ContinueGame() {
        pausePanel.SetActive(false);
        GameManager.Instance.PauseUnpauseTheGame();
    }

    public void SaveGame() {
        SaveGameManager.Instance.SaveTheGame();
    }

    public void ClearSave() {
        PlayerPrefs.DeleteAll();
    }

    public void Options() {

    }

    public void ExitTheGame() {
        Application.Quit();
    }
}
