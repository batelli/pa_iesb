﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    static GameManager instance;
    public static GameManager Instance { get { return instance; } }
    bool theGameIsPaused = false;


    private void Awake() {
        instance = this;
    }

    public void PauseUnpauseTheGame() {
        if (theGameIsPaused) {
            Time.timeScale = 1;
        }
        else {
            Time.timeScale = 0;
            UIManager.Instance.PauseTheGame();
        }

        theGameIsPaused = !theGameIsPaused;
    }
}
