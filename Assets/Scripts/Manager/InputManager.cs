﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    static InputManager instance;
    public static InputManager Instance { get { return instance; } }

    public KeyCode jumpKey = KeyCode.Space;
    public KeyCode actionKey = KeyCode.E;
    public KeyCode weaponKey = KeyCode.Q;


    private void Awake() {
        instance = this;
    }
}
