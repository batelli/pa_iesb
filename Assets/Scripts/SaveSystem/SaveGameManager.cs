﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveGameManager : MonoBehaviour
{
    static SaveGameManager instance;
    public static SaveGameManager Instance { get { return instance; } }

    public delegate void SaveLoadGame();
    public SaveLoadGame LoadGame;
    public SaveLoadGame SaveGame;


    private void Awake() {
        instance = this;
        StartCoroutine(WaitToLoadTheGame());
    }

    IEnumerator WaitToLoadTheGame() {
        yield return new WaitForSecondsRealtime(0.1f);
        if (LoadGame != null)
            LoadGame();
    }

    public void SaveTheGame() {
        if (SaveGame != null)
            SaveGame();
    }
}
