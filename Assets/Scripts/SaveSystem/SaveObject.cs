﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SaveObject : SaveBasic
{
    public UnityEvent hasSaved;
    public UnityEvent notSaved;
    public int objectWasSaved; //0-falso, 1-verdadeiro


    public override void LoadTheGame() {
        objectWasSaved = PlayerPrefs.GetInt(saveID, 0);
        if (objectWasSaved == 1)
            hasSaved.Invoke();
        else
            notSaved.Invoke();
    }

    public override void SaveTheGame() {
        if (objectWasSaved == 1)
            PlayerPrefs.SetInt(saveID, 1);
    }

    public override void SaveWasTriggered() {
        objectWasSaved = 1;
    }
}
