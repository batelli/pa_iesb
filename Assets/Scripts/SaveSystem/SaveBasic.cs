﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveBasic : MonoBehaviour
{
    public string saveID;


    protected virtual void Start() {
        SaveGameManager.Instance.SaveGame += SaveTheGame;
        SaveGameManager.Instance.LoadGame += LoadTheGame;
    }

    protected virtual void OnEnable() {
        if (!SaveGameManager.Instance)
            return;

        SaveGameManager.Instance.SaveGame += SaveTheGame;
        SaveGameManager.Instance.LoadGame += LoadTheGame;
    }

    protected virtual void OnDisable() {
        SaveGameManager.Instance.SaveGame -= SaveTheGame;
        SaveGameManager.Instance.LoadGame -= LoadTheGame;
    }

    public virtual void SaveTheGame() { //Salvar o jogo - fazer os procedimentos para o objeto

    }

    public virtual void LoadTheGame() { //Carregar o que foi salvo e tomar as devidas providências
        
    }

    public virtual void SaveWasTriggered() {

    }
}
