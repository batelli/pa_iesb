﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum CharacterClass {
    Warrior,
    Wizard,
    Rogue,
    Barbarian,
    Monk,
    Cleric,
    Ranger,
    Paladin,
    Sorcerer,
    Necromancer,
    None,
}

public enum CharacterRace { Human, Dwarf, Elf, Orc, Gnome, Halfling, Fairy, DragonBlood }

public enum CharacterSkill {
    None, Cleave, Fireball, Steal, MagicMissile, Heal, MegaJump, BarrelRoll, Bless, Poison,
    FeatherFall, IceStorm, FastArrow, FastFists
}

public class RPGHero : RPGCharacter
{
    public CharacterClass charClass;
    public CharacterRace charRace;

    public List<CharacterSkill> skills = new List<CharacterSkill>();
}
