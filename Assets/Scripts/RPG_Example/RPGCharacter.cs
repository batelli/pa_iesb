﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RPGCharacter : MonoBehaviour
{
    public string characterName = "Armando";
    public float weight, height;
    public int attMax = 30;
    public int attMin = 1;


    [Range (1, 30)]
    private int strength, dexterity, constitution, intelligence, wisdom, charisma;

    public int str { get { return strength; } set { strength = Mathf.Clamp(value, attMin, attMax); } }
    public int dex { get { return dexterity; } set { dexterity = Mathf.Clamp(value, attMin, attMax); } }
    public int con { get { return constitution; } set { constitution = Mathf.Clamp(value, attMin, attMax); } }
    public int ilg { get { return intelligence; } set { intelligence = Mathf.Clamp(value, attMin, attMax); } }
    public int wis { get { return wisdom; } set { wisdom = Mathf.Clamp(value, attMin, attMax); } }
    public int cha { get { return charisma; } set { charisma = Mathf.Clamp(value, attMin, attMax); } }
}
