﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruct : MonoBehaviour
{
    public float timeToDestroy = 2f;
    float timer = 0;


    private void Update() {
        timer+=Time.deltaTime;
        if (timer > timeToDestroy)
            Destroy(gameObject);
    }
}
