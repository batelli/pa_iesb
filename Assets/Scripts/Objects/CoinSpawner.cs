﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawner : MonoBehaviour
{
    public int maxQtt=5;
    public int minQtt=1;
    public float[] chance;


    public void SpawnCoinsNow() {
        int qtt = Random.Range(minQtt, maxQtt+1);
        for (int i = 0; i < qtt; i++) {
            Instantiate(Resources.Load(GetCoinIndex()), transform.position + new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f)), 
                Quaternion.identity);
        }
    }

    string GetCoinIndex() {
        float maxChance = 0;
        for (int i = 0; i < chance.Length; i++)
            maxChance += chance[i];

        float rand = Random.Range(0f, maxChance);
        float accChance = 0;
        int index = 0;

        for (int i = 0; i < chance.Length; i++)
        {
            accChance += chance[i];
            if (rand <= accChance) {
                index = i;
                break;
            }
        }

        switch (index)
        {
            case 1:
                return "Coin_Silver";
            case 2:
                return "Coin_Gold";
            default:
                return "Coin_Bronze";
        }
    }
}
