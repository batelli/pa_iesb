﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CoinType { BRONZE = 1, SILVER = 5, GOLD = 10 }


public class Coin : MonoBehaviour
{
    public CoinType coinType = CoinType.BRONZE;


    public void GiveCoin() {
        Player.Instance.AddMoney((int)coinType);
    }
}
