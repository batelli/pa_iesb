﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObject : MonoBehaviour
{
    public Vector3 speed = Vector3.up;

    void Update()
    {
        transform.Rotate(speed * Time.deltaTime);
    }
}
