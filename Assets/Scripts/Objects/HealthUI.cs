﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthUI : MonoBehaviour
{
    public Image healthSprite;


    private void Awake() {
        ComplexLivingBeing being = GetComponent<ComplexLivingBeing>();
        if (being)
            being.BeingWasDamaged += UpdateHealth;
    }

    public void UpdateHealth(float currentHealth) {
        healthSprite.fillAmount = currentHealth;
    }
}
