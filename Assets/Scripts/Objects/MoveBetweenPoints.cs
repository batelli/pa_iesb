﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBetweenPoints : MonoBehaviour
{
    public Transform target;
    public Transform[] pointsToMove;
    public float speed = 2f;
    int currentIndex = 0;
    int futureIndex = 1;
    float timer=0;
    float dist=0;


    private void Awake() {
        if (pointsToMove.Length < 2) {
            Destroy(this);
            return;
        }

        dist = Vector3.Distance(pointsToMove[currentIndex].position, pointsToMove[futureIndex].position);
    }

    private void Update() {
        timer += Time.deltaTime;
        float pathCompleted = timer * speed / dist;
        target.position = Vector3.Lerp(pointsToMove[currentIndex].position, pointsToMove[futureIndex].position, pathCompleted);

        if (pathCompleted > 1)
            ChangeRef();
    }

    void ChangeRef() {
        timer = 0;
        currentIndex = futureIndex;
        futureIndex++;
        if (futureIndex >= pointsToMove.Length)
            futureIndex = 0;

        dist = Vector3.Distance(pointsToMove[currentIndex].position, pointsToMove[futureIndex].position);
    }

}
