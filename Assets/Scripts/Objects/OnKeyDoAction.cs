﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnKeyDoAction : MonoBehaviour
{
    public UnityEvent actionEvent;


    private void OnTriggerStay(Collider other) {
        if (Input.GetKeyDown(InputManager.Instance.actionKey))
            actionEvent.Invoke();
    }
}
