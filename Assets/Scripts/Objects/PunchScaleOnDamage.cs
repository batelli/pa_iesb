﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PunchScaleOnDamage : MonoBehaviour
{
    public GameObject mesh;


    private void Awake() {
        ComplexLivingBeing being = GetComponent<ComplexLivingBeing>();
        if (being)
            being.BeingWasDamaged += OnDamage;
    }

    public void OnDamage(float health) {
        iTween.PunchScale(mesh, iTween.Hash("amount", new Vector3(1f, 1f, 1f), "time", 1f));
    }
}
