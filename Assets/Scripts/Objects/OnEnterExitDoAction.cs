﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OnEnterExitDoAction : MonoBehaviour
{
    public string[] availableTags;
    public UnityEvent enterEvent;
    public UnityEvent exitEvent;



    private void OnTriggerEnter(Collider other) {
        for (int i = 0; i < availableTags.Length; i++) {
            if (other.CompareTag (availableTags[i])) {
                enterEvent.Invoke();
                break;
            }
        }
    }

    private void OnTriggerExit(Collider other) {
        for (int i = 0; i < availableTags.Length; i++) {
            if (other.CompareTag (availableTags[i])) {
                exitEvent.Invoke();
                break;
            }
        }
    }
}
