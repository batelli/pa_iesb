﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeColorOnDamage : MonoBehaviour
{
    public Color damageColor = Color.red;
    Color defaultColor;
    public float blinkTime = 0.25f;
    public GameObject target;



    private void Awake() {
        ComplexLivingBeing being = GetComponent<ComplexLivingBeing>();
        if (being)
            being.BeingWasDamaged += OnDamage;

        defaultColor = target.GetComponent<Renderer>().material.color;
    }

    public void OnDamage(float health) {
        iTween.ValueTo(gameObject, iTween.Hash("from", target.GetComponent<Renderer>().material.color, 
                                            "to", damageColor,
                                            "time", blinkTime,
                                            "easetype", iTween.EaseType.easeOutCubic,
                                            "onupdate", "UpdateMeshColor",
                                            "oncomplete", "BackToOriginalColor"));
    }

    public void BackToOriginalColor() {
        iTween.ValueTo(gameObject, iTween.Hash(
                                            "from", target.GetComponent<Renderer>().material.color,
                                            "to", defaultColor,
                                            "time", blinkTime,
                                            "easetype", iTween.EaseType.easeInCubic,
                                            "onupdate", "UpdateMeshColor"));
    }

    public void UpdateMeshColor(Color newColor) {
        target.GetComponent<Renderer>().material.color = newColor;
    }
}
