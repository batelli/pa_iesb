﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShakeCameraOnDamage : MonoBehaviour
{
    public GameObject myCamera;


    private void Awake() {
        ComplexLivingBeing being = GetComponent<ComplexLivingBeing>();
        if (being)
            being.BeingWasDamaged += OnDamage;
    }

    public void OnDamage(float health) {
        iTween.PunchRotation(myCamera, iTween.Hash("time", 1f, "z", 15));
    }
}
