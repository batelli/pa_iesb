﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDisable : MonoBehaviour
{   
    public float timeToDisable = 0.2f;
    float timer;


    void OnEnable() {
        timer = 0;
    }

    private void Update() {
        timer+=Time.deltaTime;
        if (timer > timeToDisable)
            gameObject.SetActive(false);
    }
}
