﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Dialogue000", menuName = "RPG Assets/New Dialogue")]
public class Dialogue : ScriptableObject
{
    public string[] lineContent;
    public float[] lineDuration;
    public float[] lineDelay;
}
