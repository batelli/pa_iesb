﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum GearPieceType { BOOTS, ACCESSORIES, PANTS, ARMOR, HEADGEAR, SHIELD, WEAPON }

[CreateAssetMenu(fileName = "Gear", menuName = "RPG Assets/Gear Basic Piece")]
public class GearBase : ScriptableObject {
    public Sprite gearImage;
    public string gearName;
    public int gearID;
    public GearPieceType gearType;
    public int baseDefense = 0;
    public int baseAttack = 0;
    public int weight = 1;
    public int cost = 1;
}
